<?php
	$valoresIdioma = array(); 
	$valoresIdioma['home_titulo_noticia']				= 'Noticias';
	$valoresIdioma['home_titulo_ciae_enlos_medio'] 		= 'CIAE en los Medios';
	$valoresIdioma['home_titulo_publicaciones'] 		= 'Publicaciones';
	$valoresIdioma['home_titulo_documentos_trabajo'] 	= 'Documentos de Trabajo';
	$valoresIdioma['home_titulo_agenda'] 				= 'Agenda';
	$valoresIdioma['pie_mesa_central'] 					= 'Mesa Central';
	
	
	$valoresIdioma['pie_comunicaciones'] 				= 'Comunicaciones';
	$valoresIdioma['pie_privacidad'] 					= 'Privacidad y uso - Todos los derechos reservados';
	$valoresIdioma['contacto_como_llegar'] 				= 'Como llegar';
	$valoresIdioma['contacto_formulario'] 				= 'Formulario de Contacto';
	$valoresIdioma['contacto_ver_mapa'] 				= 'Agrandar';
	$valoresIdioma['header_mapa'] 						= 'Mapa del sitio';
	$valoresIdioma['header_anexos'] 					= 'Email y Anexos CIAE';
	$valoresIdioma['header_contacto'] 					= 'Contacto';
	$valoresIdioma['header_buscador'] 					= 'Buscador';
	$valoresIdioma['bloques_personas'] 					= 'Investigadores';
	$valoresIdioma['bloques_publicaciones'] 			= 'Publicaciones';
	$valoresIdioma['bloques_proyectos'] 				= 'Proyectos';
	$valoresIdioma['bloques_areas']		 				= '�reas de investigaci�n';
	$valoresIdioma['bloques_ver_mas'] 					= 'Ver m�s';
	$valoresIdioma['bloques_cierre'] 					= 'Ocultar Listado';
	
	$valoresIdioma['links_info_publicacion'] 			= 'Ver publicaci�n';
	$valoresIdioma['menu_boletin'] 						= 'Bolet�n';
	$valoresIdioma['general_mantencion'] 				= 'Disculpe las molestias, pero el sistema se encuentra en mantenci�n, por favor int�ntelo m�s tarde';
	$valoresIdioma['general_no_permiso_menu'] 			= 'Ud no tiene permisos para acceder a esta funcionalidad.';
	$valoresIdioma['general_no_permiso_acceso'] 		= 'Acceso No Autorizado';
	
	$valoresIdioma['traducir_google']			 		= 'Traducir via traductor de google';
	$valoresIdioma['general_404_no_found'] 				= '<b>P�gina no encontrada</b><br><br>La p�gina que usted busca no existe, ya no est� disponible o cambi� de direcci�n.<br><br>Ir al home <a href='.VarConfig::path_site_www.'>'.VarConfig::path_site_www.'</a>';
	 
	
	$valoresIdioma['publicaciones_filtro_todo'] 		= 'Todo';
	$valoresIdioma['publicaciones_ver_detalle'] 		= 'Ver detalle publicaci�n';
	$valoresIdioma['general_no_hay_elementos'] 			= 'No hay elementos disponibles';
	$valoresIdioma['noticias_filtro_agno_mes'] 			= 'Filtrar por fechas';
	$valoresIdioma['trabaja_con_nosotros'] 				= 'Trabaja con nosotros';
	$valoresIdioma[''] = '';
	$valoresIdioma[''] = '';
	$valoresIdioma[''] = '';
	
	
	$valoresIdioma['date_format_mysql']				= '%d-%m-%Y'; 
	$valoresIdioma['home_eventos_titulo'] 			= 'Eventos & Seminarios'; 
	$valoresIdioma['home_publicaciones_titulo'] 	= $valoresIdioma['bloques_publicaciones'];
	$valoresIdioma['home_noticias_titulo'] 			= 'Noticias y Eventos';
	$valoresIdioma['home_eventos_more'] 			= 'info';
	$valoresIdioma['home_uchile_link'] 				= 'Instituci�n albergante'; 
	$valoresIdioma['home_icm_link'] 				= 'Una iniciativa milenio';  
	$valoresIdioma['home_design_by'] 				= 'Desarrollado & Dise�ado por';
	$valoresIdioma['home_rights'] 					= 'Derechos Reservados';

	$valoresIdioma['general_volver'] 				= 'Volver';
	$valoresIdioma['general_nombre'] 				= 'Nombre';
	$valoresIdioma['general_direccion'] 			= 'Direcci�n';
	$valoresIdioma['general_ciudad'] 				= 'Ciudad';
	$valoresIdioma['general_pais'] 					=' Pa�s';
	$valoresIdioma['general_profesion'] 			= 'Profesi�n';
	$valoresIdioma['general_actividad'] 			= 'Actividad';
	$valoresIdioma['general_telefono'] 				= 'Tel�fono';
	$valoresIdioma['general_fax'] 					= 'Fax';
	$valoresIdioma['general_email'] 				= 'E-mail';
	$valoresIdioma['general_comentario'] 			= 'Comentarios o consulta';
	$valoresIdioma['general_mensaje'] 				= 'Mensaje';
	$valoresIdioma['contact_requiere'] 				= 'Ingrese correctamente ';  
	$valoresIdioma['contact_enviar_simple'] 		= 'Enviar';
	$valoresIdioma['contact_enviar'] 				= $valoresIdioma['contact_enviar_simple'].' '.$valoresIdioma['contact_comentario']; 
	$valoresIdioma['contact_email_subject'] 		= 'Contacto';
	$valoresIdioma['contact_firma'] 				= 'Gracias por escribirnos, estaremos en contacto muy pronto';
	$valoresIdioma['contacto_mensaje_enviado'] 		= 'Su mensaje fue ingresado exit�samente';
	
	$valoresIdioma['formulario_alerta_base'] 		= 'Debe ingresar ';

	$valoresIdioma['agenda_fecha_inicio']			= 'Fecha Inicio'; 	
	$valoresIdioma['agenda_fecha_termino']			= 'Fecha T�rmino'; 	
	$valoresIdioma['agenda_lugar']					= 'Lugar'; 	
	$valoresIdioma['agenda_patrocina']				= 'Patrocina'; 	
	$valoresIdioma['agenda_consulta']				= 'Consultas'; 	
	$valoresIdioma['agenda_organiza']				= 'Organiza'; 	
	$valoresIdioma['agenda_dias_lista']				= Array ( 'lu' => 'Lunes','ma'=>'Martes','mi'=>'Mi�rcoles','Ju'=>'Jueves','vi'=>'Viernes','sa'=>'S�bado','do'=>'Domingo');
	$valoresIdioma['agenda_meses_lista'] 			= array('Jan'=>'Enero', 'Feb'=>'Febrero', 'Mar'=>'Marzo', 'Apr'=>'Abril', 'May'=>'Mayo', 'Jun'=>'Junio', 'Jul'=>'Julio', 'Aug'=>'Agosto', 'Sept'=>'Septiembre', 'Oct'=>'Octubre', 'Nov'=>'Noviembre', 'Dec'=>'Diciembre');
	 

	$valoresIdioma['proyectos_area']				= "�rea de Investigaci�n";
	$valoresIdioma['proyectos_tipo_proyecto']		= "Tipo de Proyecto";
	$valoresIdioma['proyectos_url']					= "Url";
	$valoresIdioma['proyectos_antecedentes']		= "Antecedentes";
	$valoresIdioma['proyectos_objetivos']			= "Objetivos";
	$valoresIdioma['proyectos_metodologia']			= "Metodolog�a";
	$valoresIdioma['proyectos_investigadores']		= "Investigadores";
	$valoresIdioma['proyectos_periodo']				= "Per�odo de ejecuci�n";
	$valoresIdioma['proyectos_financiamiento']		= "Fuente de financiamiento";
	$valoresIdioma['proyectos_productos']			= "Productos"; 
	$valoresIdioma['proyectos_web'] 				= "P�gina Web"; 
	$valoresIdioma['proyectos_colaboradores'] 		= "Colaboradores";
	
	
	$valoresIdioma['publicaciones_titulo'] 			= "T�tulo"; 
	$valoresIdioma['publicaciones_menu_titulo'] 	= "Publicaciones";
	$valoresIdioma['publicaciones_numero'] 			= 'N�mero';
	$valoresIdioma['publicaciones_mes'] 			= 'Mes';
	$valoresIdioma['publicaciones_agno'] 			= 'A�o';
	$valoresIdioma['publicaciones_autores'] 		= 'Autores';
	$valoresIdioma['publicaciones_resumen'] 		= 'Abstract';
	$valoresIdioma['publicaciones_descarga'] 		= 'Descargar Documento';
	$valoresIdioma['publicaciones_comentarios'] 	= 'Comentarios sobre el documento';
	$valoresIdioma['publicaciones_filtro_agno'] 	= 'Filtrar por a�os';
	$valoresIdioma['publicaciones_buscador_apellidos'] 	= 'Apellidos Autores';
	$valoresIdioma['publicaciones_tipo'] 			= 'Tipo de publicaci�n';
	$valoresIdioma['publicaciones_menu_lateral']			= 'Lee sobre nuestras publicaciones';
	
	
	$valoresIdioma['buscador_buscar'] 				= 'Buscar';
	$valoresIdioma['buscador_autor'] 				= 'Autor'; 
	$valoresIdioma['buscador_palabra_clave']		= 'Palabras claves';
	

?>