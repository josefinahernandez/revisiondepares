<?php
	$valoresIdioma 		= array();

	$valoresIdioma['home_titulo_noticia']				= 'News';
	$valoresIdioma['home_titulo_ciae_enlos_medio'] 		= 'CIAE in the Press';
	$valoresIdioma['home_titulo_publicaciones'] 		= 'Publications';
	$valoresIdioma['home_titulo_documentos_trabajo'] 	= 'Working Papers';
	$valoresIdioma['home_titulo_agenda'] 				= 'Calendar';
	$valoresIdioma['pie_mesa_central'] 					= 'Main office';
	$valoresIdioma['pie_comunicaciones'] 				= 'Communications';
	$valoresIdioma['pie_privacidad'] 					= 'Privacy and use  - All rights reserved ';
	$valoresIdioma['header_mapa'] 						= 'Map site';
	$valoresIdioma['header_anexos'] 					= 'Email & Telephone CIAE';
	$valoresIdioma['header_contacto'] 					= 'Contact';
	$valoresIdioma['header_buscador'] 					= 'Search';
	$valoresIdioma['bloques_personas'] 					= 'Researchers';
	$valoresIdioma['bloques_publicaciones'] 			= 'Publications';
	$valoresIdioma['bloques_proyectos'] 				= 'Projects';
	$valoresIdioma['bloques_areas']		 				= 'Research Areas';
	$valoresIdioma['bloques_ver_mas'] 					= 'See more';
	$valoresIdioma['bloques_cierre'] 					= 'Hide List';
	$valoresIdioma['trabaja_con_nosotros'] 				= 'Work with us';
	
	$valoresIdioma['links_info_publicacion'] 			= 'See publication';
	$valoresIdioma['menu_boletin'] 						= 'Bulletin';
	$valoresIdioma['general_mantencion'] 				= 'We are sorry, but the system is in maintenance, please try later.';
	$valoresIdioma['general_no_permiso_menu'] 			= 'You do not have permission to access this application.';
	$valoresIdioma['general_no_permiso_acceso'] 		= 'Unauthorized Access'; 
	
	$valoresIdioma['traducir_google'] 					= 'Translate using google translator';
	
	$valoresIdioma['general_404_no_found'] 				= '<b>Page not found</b><br><br>The page you are looking for does not exist, no longer available or changed direction.<br><br>Go to home <a href='.VarConfig::path_site_www.'>'.VarConfig::path_site_www.'</a>'; 
	 
	$valoresIdioma['contacto_como_llegar'] 				= 'Location';
	$valoresIdioma['contacto_formulario'] 				= 'Contact Form';
	$valoresIdioma['contacto_ver_mapa'] 				= 'Zoom in';
	$valoresIdioma['contacto_mensaje_enviado'] 			= 'Your message was sent successfully';
	$valoresIdioma['general_no_hay_elementos'] 			= 'No elements available';
	
	$valoresIdioma['date_format_sql']					= '%Y-%m-%d'; 
	$valoresIdioma['home_eventos_titulo'] 				= 'Events & Seminars'; 
	$valoresIdioma['home_publicaciones_titulo'] 		= 'Publications';
	$valoresIdioma['home_noticias_titulo'] 				= 'News';
	$valoresIdioma['home_eventos_more'] 				= 'info'; 
	$valoresIdioma['home_uchile_link'] 					= 'Host Institution'; 
	$valoresIdioma['home_icm_link'] 					= 'A Millenium Scientific Initiative'; 
	$valoresIdioma['home_design_by'] 					= 'Developed & Designed by';
	$valoresIdioma['home_rights'] 						= 'All Rights Reserved';	
	$valoresIdioma['general_volver'] 					= 'Back';
	$valoresIdioma['general_nombre'] 					= 'Name'; 
	$valoresIdioma['general_direccion'] 				= 'Address'; 
	$valoresIdioma['general_ciudad'] 					= 'City'; 
	$valoresIdioma['general_mensaje'] 					= 'Message';
	$valoresIdioma['general_pais'] 						= 'Country'; 
	$valoresIdioma['general_profesion']					= 'Profession'; 
	$valoresIdioma['general_actividad']					= 'Activity'; 
	$valoresIdioma['general_telefono'] 					= 'Telephone'; 
	$valoresIdioma['general_fax'] 						= 'Fax'; 
	$valoresIdioma['general_email'] 					= 'E-mail'; 
	$valoresIdioma['contact_comentario'] 				= 'Commentary'; 
	$valoresIdioma['contact_requiere'] 					= 'Insert correctly'; 
	$valoresIdioma['contact_enviar_simple'] 			= 'Send'; 
	$valoresIdioma['contact_enviar'] 					= $valoresIdioma['contact_enviar_simple'].' '.$valoresIdioma['contact_comentario']; 
	$valoresIdioma['contact_email_subject'] 			= 'Contact'; 
	$valoresIdioma['formulario_alerta_base'] 			= 'Must enter ';
	
	$valoresIdioma['contact_firma'] 					= 'Thanks for you suggestions we contact you very soon'; 	

	$valoresIdioma['agenda_fecha_inicio']				= 'Start Date'; 	
	$valoresIdioma['agenda_fecha_termino']				= 'End Date'; 	
	$valoresIdioma['agenda_lugar']						= 'Place'; 	
	$valoresIdioma['agenda_patrocina']					= 'Sponsorship'; 	
	$valoresIdioma['agenda_consulta']					= 'Consultations'; 	
	$valoresIdioma['agenda_organiza']					= 'Organized'; 	
	$valoresIdioma['agenda_dias_lista']					= array('lu'		=>'Monday', 'ma'		=>'Tuesday', 'mi'		=>'Wednesday', 'ju'		=>'Thursday', 'vi'		=>'Friday', 'sa'		=>'Saturday', 'do'		=>'Sunday');
	$valoresIdioma['agenda_meses_lista'] 				= array ( 'Jan' 		=> 'January', 'Feb' 		=> 'February', 'Mar' 		=> 'March', 'Apr' 		=> 'April', 'May' 		=> 'May', 'Jun' 		=> 'June', 'Jul' 		=> 'July', 'Aug' 		=> 'August', 'Sept' 		=> 'September', 'Oct' 		=> 'October', 'Nov' 		=> 'November' , 'Dec' 		=> 'December');
	$valoresIdioma['noticias_filtro_agno_mes'] 			= 'Filter by date';
	  
	$valoresIdioma['proyectos_area'] 					= "Research Area";
	$valoresIdioma['proyectos_tipo_proyecto'] 			= "Project Type";
	$valoresIdioma['proyectos_url'] 					= "Url";
	$valoresIdioma['proyectos_antecedentes'] 			= "Background";
	$valoresIdioma['proyectos_objetivos'] 				= "Targets";
	$valoresIdioma['proyectos_metodologia'] 			= "Methodology";
	$valoresIdioma['proyectos_investigadores'] 			= "Researchers";
	$valoresIdioma['proyectos_periodo'] 				= "Implementation period";
	$valoresIdioma['proyectos_financiamiento'] 			= "Funding";
	$valoresIdioma['proyectos_productos'] 				= "Products";
	$valoresIdioma['proyectos_web'] 					= "Web Page";
	$valoresIdioma['proyectos_colaboradores'] 			= "Collaborators"; 
	
	$valoresIdioma['publicaciones_titulo'] 				= "Title"; 
	$valoresIdioma['publicaciones_menu_titulo'] 		= "Publications"; 
	$valoresIdioma['publicaciones_numero'] 				= 'Number';
	$valoresIdioma['publicaciones_mes'] 				= 'Month';
	$valoresIdioma['publicaciones_agno'] 				= 'Year';
	$valoresIdioma['publicaciones_autores'] 			= 'Authors';
	$valoresIdioma['publicaciones_resumen'] 			= 'Abstract';
	$valoresIdioma['publicaciones_descarga'] 			= 'Download Document';
	$valoresIdioma['publicaciones_comentarios']			= 'Comments about document';
	$valoresIdioma['publicaciones_filtro_agno'] 		= 'Filter by year';
	$valoresIdioma['publicaciones_filtro_todo'] 		= 'All';
	$valoresIdioma['publicaciones_ver_detalle'] 		= 'See publication';
	$valoresIdioma['publicaciones_buscador_apellidos'] 	= 'Authors Lastname';
	$valoresIdioma['publicaciones_tipo'] 				= 'Publication type';
	$valoresIdioma['publicaciones_menu_lateral']		= 'Read about our publications';
	
	$valoresIdioma['buscador_buscar'] 					= 'Search';
	$valoresIdioma['buscador_autor'] 					= 'Author'; 
	$valoresIdioma['buscador_palabra_clave']			= 'Keywords';
	
	

?>