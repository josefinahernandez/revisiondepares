

 <table class="tabla_noborder_admin"      >
    	<tr><th colspan="2">Evaluaci&oacute;n</th></tr>
        <tr><td colspan="2"><strong>Como autor, �cu�l es su impresi�n general de los cr�ticos?</strong></td></tr>
        <tr><td>Constructivo </td><td><input type="radio" /> Si <input type="radio" /> No</td></tr>
        <tr><td>Constructivo </td><td><input type="radio" /> Si <input type="radio" /> No</td></tr>
        <tr><td>Entrega siempre sugerencias detalladas  </td><td><input type="radio" /> Si <input type="radio" /> No</td></tr>
        <tr><td>Entrega sugerencias sensatas </td><td><input type="radio" /> Si <input type="radio" /> No</td></tr>
        <tr><td>Demasiado cort�s </td><td><input type="radio" /> Si <input type="radio" /> No</td></tr>
        <tr><td> 	No muy �til </td><td><input type="radio" /> Si <input type="radio" /> No</td></tr>
        <tr><td>Muy negativo </td><td><input type="radio" /> Si <input type="radio" /> No</td></tr>
        <tr><td>Le falta sustancia</td><td><input type="radio" /> Si <input type="radio" /> No</td></tr>
        <tr><td>No entendie los puntos que esta tratando de hacer </td><td><input type="radio" /> Si <input type="radio" /> No</td></tr> 
        
        <tr><td colspan="2" style=" border-bottom:1px dotted #666666">&nbsp;</td></tr>
        <tr><td colspan="2"><strong>Por favor, ordene las siguientes, en la escala de 1 a 7, de acuerdo a su grado de pertinencia en la elecci�n de la calificaci�n que le diste a la revisi�n de sus textos</strong></td></tr>
        <tr><td>Amistoso </td><td><input type="radio" /> 1 <input type="radio" /> 2 <input type="radio" /> 3 <input type="radio" /> 4 <input type="radio" /> 5 <input type="radio" /> 6 <input type="radio" /> 7</td></tr>
        <tr><td>La claridad de la revisi�n </td><td><input type="radio" /> 1 <input type="radio" /> 2 <input type="radio" /> 3 <input type="radio" /> 4 <input type="radio" /> 5 <input type="radio" /> 6 <input type="radio" /> 7</td></tr>
        <tr><td>Buenos puntos planteados </td><td><input type="radio" /> 1 <input type="radio" /> 2 <input type="radio" /> 3 <input type="radio" /> 4 <input type="radio" /> 5 <input type="radio" /> 6 <input type="radio" /> 7</td></tr>
        <tr><td>Correcciones �tiles al estilo</td><td><input type="radio" /> 1 <input type="radio" /> 2 <input type="radio" /> 3 <input type="radio" /> 4 <input type="radio" /> 5 <input type="radio" /> 6 <input type="radio" /> 7</td></tr>
        <tr><td>Falta amabilidad</td><td><input type="radio" /> 1 <input type="radio" /> 2 <input type="radio" /> 3 <input type="radio" /> 4 <input type="radio" /> 5 <input type="radio" /> 6 <input type="radio" /> 7</td></tr>
        <tr><td> 	Falta claridad </td><td><input type="radio" /> 1 <input type="radio" /> 2 <input type="radio" /> 3 <input type="radio" /> 4 <input type="radio" /> 5 <input type="radio" /> 6 <input type="radio" /> 7</td></tr>
        <tr><td> 	Falta buenos puntos en la revisi�n</td><td><input type="radio" /> 1 <input type="radio" /> 2 <input type="radio" /> 3 <input type="radio" /> 4 <input type="radio" /> 5 <input type="radio" /> 6 <input type="radio" /> 7</td></tr>
        <tr><td>Falta correcciones �tiles al estilo</td><td><input type="radio" /> 1 <input type="radio" /> 2 <input type="radio" /> 3 <input type="radio" /> 4 <input type="radio" /> 5 <input type="radio" /> 6 <input type="radio" /> 7</td></tr>
        
        
        <tr><td colspan="2" style=" border-bottom:1px dotted #666666">&nbsp;</td></tr>
        <tr><td colspan="2"><strong>Comentario</strong></td></tr>
        <tr><td colspan="2"><textarea  style="width:99%"></textarea></td></tr>
        </table>