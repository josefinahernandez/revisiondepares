<!-- START BLOCK : nuevo_usuario -->
Se le ha creado una cuenta de usuario para el sitio 
<!-- END BLOCK : nuevo_usuario -->
<!-- START BLOCK : edicion_usuario -->
Se han modificados algunos datos de sus cuenta de usuario para el sitio 
<!-- END BLOCK : edicion_usuario -->
<strong>{sitio_nombre}</strong>

<strong>Nombre 					:</strong>&nbsp;&nbsp;&nbsp;&nbsp; {nombre} 
 <strong>Email 					:</strong> &nbsp;&nbsp;&nbsp;&nbsp;{email} 

 <strong>Nombre de Usuario 		:</strong> &nbsp;&nbsp;&nbsp;&nbsp;{username} 
<!-- START BLOCK : password_usuario -->
 <strong>Clave            		:</strong>&nbsp;&nbsp;&nbsp;&nbsp; {password} 
<!-- END BLOCK : password_usuario --> 
 <strong>Estado Cuenta 					:</strong> &nbsp;&nbsp;&nbsp;&nbsp;{activo}  
 
 
Este email le permitir&aacute; recuperar sus datos de usuario en caso de  olvido, <strong>NO LO BORRE</strong>. 

Si usted desea cambiar su clave, ingrese al sistema con los datos que  aqu&iacute; se proporcionan y dir&iacute;jase al men&uacute; &ldquo;Mi cuenta&rdquo;, ubicado en la parte  superior izquierda de su pantalla. 

    Recuerde que al cambiar su clave, el Centro de  Contractos ATE le enviar&aacute; autom&aacute;ticamente un correo electr&oacute;nico de respaldo con  sus datos, por favor <strong>NO LO BORRE</strong>. Recuerde que para ubicar este correo debe  revisar tanto su &ldquo;bandeja de entrada&rdquo; como su &ldquo;bandeja de correo no deseado&rdquo;. 
 
 

