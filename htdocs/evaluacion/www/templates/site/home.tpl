	<table style=" width:640px" border="0">
	  <tr>
		<td style="width:50%" class="celda_destacada" style="text-transform:uppercase"><strong style="text-transform:uppercase"> Seminario CEA</strong>  </td> 
		<td style="width:1px"  > </td>
		<td style="width:50%" class="celda_destacada" style="text-transform:uppercase"> <strong style="text-transform:uppercase">Noticias</strong> </td>
	  </tr>
	  <tr>
		<td>&quot;<a href="../../doc/seminarios/{seminario_archivo}"><strong>{seminario_texto}</strong></a>&quot; </td>
		<td  >&nbsp;</td>
		<td><a href="../../doc/news/{noticia_archivo}"><font color="#346767"><strong>{noticia_titulo}</strong></font></a></td>
	  </tr>
	  <tr>
		<td><p align="justify" class="TEXTautores1"> Autor: 
		  {seminario_autor} </p>
		  <p class="TEXTautores1"> Fecha: 
			{seminario_dia} </p>
		  <p class="TEXTautores1">  Lugar: {seminario_lugar} </p></td>
		<td  >&nbsp;</td>
		<td><span class="TEXTautores1">{noticia_noticia} </span></td>
	  </tr>
	  <tr>
		<td colspan="3">&nbsp;</td> 
	  </tr>
	  <tr>
		<td colspan="3"  class="celda_destacada" style="text-transform:uppercase"><strong>Documentos Trabajo </strong></td>
	  </tr>
	  <TR>
		<TD height="22" colspan="3" align="center" valign="top"><div align="justify" style=" padding-left:30px">
		
		<!-- START BLOCK : bloque_documentos -->
		<li><strong>N&ordm; {numero}.</strong><br />
		<a href="{link}">{texto}</a></li>
		 <!-- END BLOCK : bloque_documentos --></TD>
	  </TR>
	  <tr>
		<td colspan="3">&nbsp;</td> 
	  </tr>
	</table>
	 