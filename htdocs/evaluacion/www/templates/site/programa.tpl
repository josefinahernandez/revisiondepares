<script>

hiddenId('navegador_ant');
hiddenId('navegador');
document.getElementById('titulo_inter').style.width = '840px';
document.getElementById('contenido_inter').style.width = '840px';

</script>
<style>

ol, ul, li {text-align:left;   }
li { padding-bottom:5px;}
.fila_programa { width:100%}
.fila_programa td { text-align:center; border: 1px solid #333333; padding: 10px 5px 10px 5px;}
.fila_programa_invitados { background-color:#06518A; color:#FFFFFF;text-align:center; }
.titulo {text-align:center; font-size:25px}
</style>
<div style="text-align:right"><a  href="index.php">&raquo; Volver </a></div>
<div  class="titulo">
<strong >JUEVES 30 DE  SEPTIEMBRE</strong></div>
<table border="0" class="fila_programa" cellspacing="0" cellpadding="0" style="width:100%">
  <tr>
    <td width="8%" nowrap valign="top"  > 
    <strong>08:15 - 08:45</strong> </td>
    <td   nowrap colspan="3"   valign="top"> <strong>Registro</strong> </td>
  </tr>
  <tr>
    <td   nowrap valign="top"  ><strong>08:45 - 10:00</strong></td>
    <td colspan="3"  valign="top"  nowrap bgcolor="#E9E9E9"> <strong>CEREMONIA INAUGURAL CIIE-CIES</strong> </td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="top"  ><strong>10:00 - 10:30</strong></td>
    <td  nowrap colspan="3"  ><strong>Caf&eacute;</strong></td>
  </tr>
  <tr>
    <td width="8%" valign="top" nowrap><strong>10:30 - 11:25</strong>       </td>
    <td  valign="top"><strong>HISTORIA DE    LA EDUCACI&Oacute;N</strong><br>
	 
	
            <ol><li> La educaci&oacute;n en la reflexi&oacute;n sociopol&iacute;tica del per&iacute;odo de restauraci&oacute;n    de la democracia chilena (1988 -2008), <strong>Juan Eduardo Garc&iacute;a-Huidobro,</strong> Roc&iacute;o Ferrada, Marcela Gil (Universidad Alberto Hurtado)</li>
            <li> La familia y la escuela: hogar    y estrategias familiares frente a la educaci&oacute;n popular, siglos XIX-XX, <strong>M&ordf;    Francisca Rengifo </strong>(Pontificia Universidad Cat&oacute;lica de Chile)<em><br> Moderadora: Sol Serrano</em></td>
    <td  valign="top"><strong>DOCENTES 1</strong><br>
      N&uacute;cleo Milenio: La Profesi&oacute;n Docente en Chile.&nbsp; Pol&iacute;ticas, Pr&aacute;cticas, Proyecciones:<br>
      <ol><li> Identidad Docente </li>
            <li> Condiciones Salariales de los    Docentes </li>
            <li> Status Docente&nbsp; </li>
            <li>Percepci&oacute;n de Eficacia Docente    y SIMCE&nbsp; <br>
      Beatrice &Aacute;valos, <strong>Danae de los R&iacute;os, Juan Pablo Valenzuela, Cristi&aacute;n    Bellei, Marcela Pardo </strong> 
      (CIAE, Universidad de Chile)<br>
      <em>Moderadora: Beatrice &Aacute;valos</em><strong><br>
      </strong> </td>
    <td  valign="top" class="fila_programa_invitados" > <p class="fila_programa_invitados"><strong>INVITADO    INTERNACIONAL</strong><br>
            <strong>EDUCACI&Oacute;N    SUPERIOR<br>
            </strong> <br>
            <strong>Daniel Levy</strong> <br>
      (State    University of New York) <br> <br>
	  &quot;Tres d&eacute;cadas de investigaci&oacute;n en educaci&oacute;n superior: reflexiones desde    una experiencia personal&quot;<br>    
          </p></td>
  </tr>
  
  <tr>
    <td width="8%" nowrap valign="top"><strong>11:30 - 13:00</strong>
         </td>
    <td  valign="top"><strong>NEUROCIENCIAS    Y COGNICI&Oacute;N</strong><br>
            <ol> <li>Efecto del estr&eacute;s sobre la plasticidad sin&aacute;ptica, la conducta y la    capacidad de aprendizaje asociativo en un modelo animal, <strong>Patricia    Casta&ntilde;eda-Pezo </strong>(UMCE), G. D&iacute;az, S. Mora, J. Fiedler (Universidad de    Chile)</li>
            <li> El rango ojo-mano durante la    lectura a primera vista y la ejecuci&oacute;n musical, <strong>Michel Cara,</strong> Paul    Molin <br>
      (Universit&eacute; de Bourgogne, France)</li>
            <li> Desarrollo de habilidades de    navegaci&oacute;n a trav&eacute;s de videojuegos basados en audio, <strong>Jaime S&aacute;nchez, </strong>Mauricio    S&aacute;enz (Universidad de Chile), Alvaro Pascual-Leone, Lotfi Merabet (Harvard    Medical School)</li>
            <li> Cerebro y aprendizaje, <strong>Anal&iacute;a Adriana Uva</strong> (Universidad Nacional de R&iacute;o Cuarto, Argentina)<br>
      <em>Moderadora: Andrea Slachevsky</em>
	  </li></ol> </td>
    <td  valign="top"><strong>ENSE&Ntilde;ANZA Y APRENDIZAJE 1</strong><br>
            <ol><li> Demanda cognitiva en las clases de matem&aacute;ticas chilena, <strong>Llery Ponce, </strong>David    Preiss, M&oacute;nica Nunes (Pontificia Universidad Cat&oacute;lica de Chile)</li>
            <li> Estructura y discurso del aula    matem&aacute;tica de segundo ciclo b&aacute;sico en Chile, <strong>David Preiss, </strong>Susana    Valenzuela (Pontificia Universidad Cat&oacute;lica de Chile)</li>
            <li> Evaluaci&oacute;n de diversas    componentes del conocimiento matem&aacute;tico necesario para ense&ntilde;ar matem&aacute;ticas en    Ense&ntilde;anza B&aacute;sica, <strong>M&ordf; Leonor Varas,</strong> Nancy Lacourly&nbsp; (Universidad de Chile)</li>
            <li> Promoci&oacute;n de competencia    cient&iacute;fica: &iquest;qu&eacute; capacidades, conocimientos y actitudes son promovidas en    pr&aacute;cticas de profesores de ciencia de educaci&oacute;n media de la regi&oacute;n de    Valpara&iacute;so?, <strong>Corina Gonz&aacute;lez-Weil</strong>, Paulina Bravo, Alejandro Abarca,    Pablo Castillo, Sebasti&aacute;n &Aacute;lvarez (Pontificia Universidad Cat&oacute;lica de    Valpara&iacute;so)<br>
            <em>Moderador: Patricio Felmer </em></li></ol>    </td>
    <td  valign="top"><strong>EDUCACI&Oacute;N    SUPERIOR 1</strong><br>
            <ol><li> &iquest;C&oacute;mo enfocar las pr&aacute;cticas curriculares en un curr&iacute;culo orientado por    competencias?, <strong>Jaime Mill&aacute;n </strong>(Universidad Cat&oacute;lica de Temuco), Maril&uacute;    Rioseco G. Hern&aacute;n Peredo L.&nbsp; </li>
            <li> Hacia la implementaci&oacute;n del    nuevo modelo educativo de la Universidad de Concepci&oacute;n: establecimiento de    perfiles de egreso en el proceso de redise&ntilde;o curricular, Jos&eacute; S&aacute;nchez, Mar&iacute;a    In&eacute;s Solar, <strong>Marcela Varas 
            </strong>(Universidad de Concepci&oacute;n) </li>
            <li> Adecuaci&oacute;n de la metodolog&iacute;a    del proceso de titulaci&oacute;n al contexto actual, Escuela de Arquitectura, PUC, <strong>Jocelyn    Morales </strong>(Pontificia Universidad Cat&oacute;lica de Chile)</li>
            <li> Transdisciplina y Educaci&oacute;n    Terciaria en el Sur de Chile; resultados de un estudio    hermen&eacute;utico-paradigmatol&oacute;gico, <strong>Nicol&aacute;s D&iacute;az</strong>, Iv&aacute;n Oliva Figueroa    (Universidad Austral de Chile)<br>
            <em>Moderador: Andr&eacute;s Bernasconi</em></li></ol></td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="top"><strong>13:00- &nbsp;14:30&nbsp;</strong></td>
    <td  nowrap colspan="3"><strong>Almuerzo    (libre)</strong></td>
  </tr>
  <tr>
    <td width="8%" valign="top" nowrap><strong>14:30 - 15:30</strong> </td>
    <td valign="top" align="center"  class="fila_programa_invitados" > <p   class="fila_programa_invitados" ><strong>INVITADO    INTERNACIONAL</strong><br>
            <strong>NEUROCIENCIAS    Y COGNICI&Oacute;N</strong><br>
            <br>
            <strong>Lotfi Merabet</strong> <br>
      (Harvard Medical School) <br> <br>
	  &quot;Plasticidad Cerebral, Investigaci&oacute;n y Aplicaciones&quot;<br>    
      </p></td>
    <td valign="top"  class="fila_programa_invitados" ><p  class="fila_programa_invitados" ><strong>INVITADO    INTERNACIONAL</strong><br>
            <strong>ENSE&Ntilde;ANZA Y    APRENDIZAJE</strong><br>
            <br>
            <strong>Vicente Talanquer</strong> <br>
      (Universidad    de Arizona)<br> <br>
	  &quot;&iquest;C&oacute;mo piensan nuestros alumnos?&quot;<br>    
      </p></td>
    <td valign="top"><strong>EDUCACI&Oacute;N SUPERIOR    2</strong><br>
            <ol><li> Acceso a    educaci&oacute;n superior y deserci&oacute;n: evidencia de una cohorte de egresados de    educaci&oacute;n media&nbsp;chilenos, Carolina    Acu&ntilde;a, <strong>Mattia Makovec</strong>, Alejandra Mizala<br>
      (Universidad de Chile)</li>
            <li> Caracterizaci&oacute;n de los j&oacute;venes    primera generaci&oacute;n en educaci&oacute;n superior. Nuevas trayectorias hacia la    equidad, Jorge Castillo, <strong>Gustavo Cabezas </strong>(UNDP)</li>
            <li> Caracterizaci&oacute;n socioecon&oacute;mica    y demogr&aacute;fica de los estudiantes de postgrado de la Universidad de Concepci&oacute;n, <strong>Magdalena Salda&ntilde;a </strong>(Universidad de Concepci&oacute;n)<br>
      <em>Moderador: &Oacute;scar Espinoza</em> </li></ol></td>
  </tr>
  
  <tr>
    <td width="8%" nowrap valign="top"><strong>15:30 - 16:00</strong></td>
    <td  nowrap colspan="3"><strong>Caf&eacute;</strong></td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="top"><strong>16:00 - 17:25</strong>
         </td>
    <td  valign="top"><strong>DOCENTES 2</strong><br>
            <ol><li> G&eacute;nero y nivel socioecon&oacute;mico de los ni&ntilde;os: expectativas del docente en    formaci&oacute;n, <strong>M&ordf; Francisca del R&iacute;o</strong>, Jaime Balladares (Universidad Diego    Portales)</li>
            <li> Percepciones de profesionales    en formaci&oacute;n de pedagog&iacute;a en educaci&oacute;n general b&aacute;sica sobre los recursos    movilizados en su inserci&oacute;n al aula, durante la pr&aacute;ctica profesional. Logros    y dificultades, <strong>Gerardo S&aacute;nchez </strong>(Universidad Aut&oacute;noma de Chile)</li>
            <li> Pr&aacute;cticas docentes ejemplares:    los casos de profesores de liceos focalizados como prioritarios de la Regi&oacute;n    de Valpara&iacute;so, Carolina Guzm&aacute;n, <strong>Ver&oacute;nica Rodr&iacute;guez </strong>(Universidad de    Valpara&iacute;so)</li>
            <li> &iquest;Qu&eacute; expectativas de la    supervisi&oacute;n de sus pr&aacute;cticas tienen los estudiantes de carreras de Pedagog&iacute;a    en Ense&ntilde;anza Media?, <strong>Horacio Walker </strong>(Universidad Diego Portales)    Carmen Montecinos (University of Wyoming) Claudio Nu&ntilde;ez, In&eacute;s Contreras,    Cristina Solis, Sylvia Rittershausen (PUC)<em> <br>
Moderador: Carlos Calvo</em></li></ol></td>
    <td  valign="top"><strong>POL&Iacute;TICA    EDUCATIVA 1</strong><br>
            <ol> <li> Caracterizaci&oacute;n de los establecimientos educacionales en Chile: la    necesidad de nuevas consideraciones, <strong>Rodrigo Roco</strong> (IREDU/CNRS Francia)</li>
            <li> &iquest;C&oacute;mo eligen escuela las    familias chilenas de estratos medios y bajos? Informaci&oacute;n, representaciones    sociales y segregaci&oacute;n, <strong>Macarena    Hern&aacute;ndez</strong>, Dagmar Raczynski<strong>
            </strong>(Asesor&iacute;as para el Desarrollo) </li>
            <li> Desigualdad de oportunidades en    logros educacionales de los estudiantes chilenos, <strong>Osvaldo Larra&ntilde;aga</strong> (UNDP), Amanda    Telias (Ministerio de Planificaci&oacute;n)</li>
            <li> Experiencias de desertores y    percepciones de&nbsp; estudiantes de Cerro    Navia acerca de abandono escolar, Oscar Espinoza PIIE y Universidad UCINF) <strong>Dante    Castillo </strong>(PIIE y Universidad UCINF), Luis Eduardo Gonz&aacute;lez (PIIE y Universidad UCINF), Eduardo Santa Cruz    (Universidad de Granada y  Universidad UCINF)<br>
            <em>Moderador: Osvaldo Larra&ntilde;aga</em></li></ol> </td>
    <td  valign="top"><strong>TIC Y    EDUCACI&Oacute;N 1</strong><br>
            <ol> <li> Creencias sobre las TICs en la formaci&oacute;n inicial de docentes: develando    similitudes y diferencias, <strong>Jos&eacute; Miguel Garrido </strong>(Pontificia Universidad    Cat&oacute;lica de Valpara&iacute;so)</li>
            <li> Concepciones del profesorado    sobre las tecnolog&iacute;as de la informaci&oacute;n y la comunicaci&oacute;n (TIC) y sus    implicaciones educativas, Susan Sanhueza, <strong>Marcelo Rioseco</strong>, Carlos    Villegas (Universidad Cat&oacute;lica del Maule), Antonio Puentes (Universidad de    Alicante, Espa&ntilde;a)</li>
            <li> Uso de TIC en la pr&aacute;ctica    educativa de docentes y estudiantes de las carreras de educaci&oacute;n de la Universidad del B&iacute;o-B&iacute;o, <strong>Nancy    Castillo</strong>, Rossana Ponce de Le&oacute;n (Universidad del B&iacute;o B&iacute;o)</li>
            <li> Desarrollo y uso de animaciones    digitales para docencia en biomedicina, <strong>Ana Mar&iacute;a Campos</strong>, Lisette    Leyton (Universidad de Chile)<br>
            <em>Moderador: Jaime S&aacute;nchez</em></li></ol> </td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="top"><strong>17:30 - 18:30</strong></td>
    <td  valign="top"  class="fila_programa_invitados" ><p  class="fila_programa_invitados" ><strong>INVITADA    INTERNACIONAL</strong><br>
            <strong>DOCENTES<br>
            </strong> <br>
            <strong>Maureen Robinson</strong> <br>
        (Kenton    Education Association, Sud &Aacute;frica)<br> <br>
	  &quot;Formaci&oacute;n Docente Inicial e Investigaci&oacute;n.&nbsp; La experiencia de Sud &Aacute;frica.&quot;<br>    
          </p></td>
    <td  nowrap valign="top"><strong>&nbsp;</strong></td>
    <td  valign="top"><strong>EDUCACI&Oacute;N    SUPERIOR 3</strong><br>
            <ol> <li> Factores que inciden en el logro acad&eacute;mico de estudiantes universitarios    de alta vulnerabilidad: estudio de caso en las Universidades de Chile y de    Concepci&oacute;n, <strong>M&ordf; In&eacute;s Solar</strong>, Luc&iacute;a    Dom&iacute;nguez, Jos&eacute; S&aacute;nchez (Universidad de Concepci&oacute;n), M&ordf; Elena Acu&ntilde;a    (Universidad de Chile)</li>
            <li> La universidad frente a la    diversidad estudiantil: Universidad Bernardo O&rsquo;Higgins, el caso de una    universidad privada inclusiva, <strong>Cecilia Assef</strong>, Sergio Becerra    (Universidad Bernardo O&rsquo;Higgins)</li>
            <li> Visual studies on the idea of the university    in Chile: higher education advertising in public spaces and students&rsquo;    graffitis, <strong>Elisabeth Simbuerger </strong>(Universidad Diego Portales)<br>
            <em>Moderador: Enrique Fern&aacute;ndez</em></li></ol></td>
  </tr>
</table>
<strong><br clear="all">
</strong><div style="text-align:right"><a  href="index.php">&raquo; Volver </a></div>
<div  class="titulo">
<strong  >VIERNES 1 DE OCTUBRE</strong></div>
<table border="0"  class="fila_programa" cellspacing="0" cellpadding="0"  style="width:100%" >
  <tr>
    <td width="8%" nowrap valign="top"> 
        <strong>8:15 - 8:45</strong> </td>
    <td  colspan="3" valign="top"><strong>Registro</strong></td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="top"><strong>8:45 - 10:25</strong></td>
    <td width="29%" valign="top"><strong>TIC Y EDUCACI&Oacute;N 2</strong><br>
            <ol><li> Diferencias    de g&eacute;nero y TICs en la educaci&oacute;n chilena, Jaime S&aacute;nchez, <strong>Claudia Mendoza </strong>(Universidad    de Chile)</li>
            <li> Midiendo el desarrollo digital    de una escuela: una propuesta conceptual y emp&iacute;rica, <strong>Christian Labb&eacute;</strong>,    Carolina Matamala (Universidad de La Frontera) y Gonzalo Donoso (Enlaces,    Mineduc)</li>
            <li> La relaci&oacute;n social en el aula    como elemento esencial para dise&ntilde;ar software educativo, <strong>Cristi&aacute;n Infante </strong>(Director    Colegio San Luis Beltr&aacute;n), Miguel Nussbaum (Pontificia Universidad Cat&oacute;lica    de Chile)</li>
            <li> Interacciones docentes en un    espacio virtual de aprendizaje, <strong>Juan Silva</strong> (Universidad de Santiago de    Chile)<br>
            <em>Moderador:    Enrique Inostroza</em></li></ol> </td>
    <td width="30%" valign="top"><strong>DOCENTES 3</strong><br>
            <ol> <li> Autoestima    profesional: competencia mediadora en el marco de la evaluaci&oacute;n docente,    Karin Wilhelm, <strong>Christian Miranda </strong>(Universidad Austral de Chile) </li>
            <li> Calidad docente y logro    escolar: enfrentando el problema de ordenamiento no aleatorio entre    caracter&iacute;sticas de profesores y alumnos, <strong>Gabriela Toledo </strong>(Universidad    de Chile)</li>
            <li> Distribuci&oacute;n inequitativa de    los nuevos docentes de mejor rendimiento, <strong>Lorena Meckes</strong>, Mart&iacute;n    Bascop&eacute; (Pontificia Universidad Cat&oacute;lica de Chile)</li>
            <li> Formaci&oacute;n permanente de    profesores: &iquest;qui&eacute;n es el formador de formadores?, <strong>Pablo Rivera</strong>,    Christian Miranda 
      (Universidad Austral de Chile)<br>
      <em>Moderador:    Abelardo Castro</em> </td>
    <td width="30%" valign="top"><strong>ENSE&Ntilde;ANZA Y APRENDIZAJE 2</strong><br>
            <ol><li>Dilemas    morales en la ense&ntilde;anza de la historia reciente que refiere a las violaciones    a los derechos humanos en Chile, M&ordf; Isabel Toledo(Universidad Diego    Portales),<strong>Abraham Magendzo </strong>(Universidad Academia de Humanismo    Cristiano) Renato Gazmuri (Universidad Diego Portales)</li>
            <li> El contexto interaccional en el    aula de educaci&oacute;n inicial: estudio comparativo en dos culturas, Nolfa Ib&aacute;&ntilde;ez,    Tatiana D&iacute;az, <strong>Sof&iacute;a Druker, </strong>Soledad Rodr&iacute;guez (Universidad    Metropolitana de Ciencias de la Educaci&oacute;n)</li>
            <li> Relaci&oacute;n entre intimidaci&oacute;n <em>(bullying)</em> y clima en la sala de clases y su influencia sobre el rendimiento de los    estudiantes, <strong>Virna Guti&eacute;rrez</strong>, M&ordf; Isabel Toledo (Universidad Diego    Portales), Abraham Magendzo (Universidad Academia de Humanismo Cristiano)<em> <br>
Moderador: David Preiss</em></td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="top"><strong>10:30 - 11:30</strong></td>
    <td width="29%" valign="top"  class="fila_programa_invitados" ><p  class="fila_programa_invitados" ><strong>INVITADO INTERNACIONAL<br>
        TIC Y EDUCACI&Oacute;N<br>
      </strong> <br>
      <strong>Neil Selwyn</strong><br>
      (London Knowledge Lab)<br> <br>
	  &ldquo;Escuela 2.0 &ndash; Explorando el papel de la escuela en la era digital&rdquo;<br>
      </p></td>
    <td width="30%" valign="top" class="fila_programa_invitados" ><p  class="fila_programa_invitados" ><strong>INVITADO INTERNACIONAL<br>
        POL&Iacute;TICA EDUCATIVA<br>
      </strong> <br>
      <strong>Miguel Urquiola</strong><br>
      (Universidad de Columbia)<br> <br>
	  &quot;Mejorar la calidad educativa: &iquest;Por qu&eacute; la competencia no ha sido una    panacea? &iquest;Puede funcionar mejor?&quot;<br>
      </p></td>
    <td width="30%" valign="top"><strong>EDUCACI&Oacute;N SUPERIOR 4</strong><br>
            <ol><li> Los    programas de doctorado en educaci&oacute;n en Chile: diagn&oacute;stico y proyecciones,    Luis Eduardo Gonz&aacute;lez, <strong>Oscar Espinoza</strong>, Cecilia Kaluf (PIIE)</li>
            <li> Innovaci&oacute;n de programas: nuevos    programas profesionales creados por universidades chilenas (1980-2008), <strong>Gonzalo    Zapata</strong> (Pontificia Universidad Cat&oacute;lica de Chile)</li>
            <li> Los determinantes que inciden    en obtener una beca para estudios de postgrado, Sebasti&aacute;n Gallegos, <strong>Paula    Bustos, </strong>Pilar Romaguera (Universidad de Chile)<br>
            <em>Moderador:    Manuel Krauskopf</em></td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="bottom"><strong>11:30 - 12:00</strong></td>
    <td  nowrap colspan="3"><strong>Caf&eacute;</strong></td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="top"><strong>12:00 - 13:30</strong></td>
    <td width="29%" valign="top"><strong>ENSE&Ntilde;ANZA Y APRENDIZAJE 3</strong><br>
            <ol><li> Aplicaci&oacute;n    del programa PEI-b&aacute;sico para promover el desarrollo cognitivo y afectivo en    pre-escolares, Natalia Salas, <strong>Cecilia Assael, </strong>David Huepe, Teresa    P&eacute;rez, Grimaldina Araya, Fernando Gonz&aacute;lez, Alejandra Morales, Rita Ar&eacute;valo,    Chetty Espinoza (Universidad Diego Portales) </li>
            <li> Coherencia local en la    producci&oacute;n escrita no narrativa de escolares de Santiago: una investigaci&oacute;n    sobre los productos escritos, conocimiento metaling&uuml;&iacute;stico y las conductas de    proceso, para informar a la did&aacute;ctica espec&iacute;fica, <strong>Soledad Concha </strong>(Universidad    Diego Portales), Soledad Aravena (Universidad Alberto Hurtado), Carmen Julia    Coloma (Universidad de Chile), 
      Ver&oacute;nica Romero (Universidad Diego Portales)</li>
            <li> &iquest;Qu&eacute; actividades realizan los    docentes de NB1 para ense&ntilde;ar a leer y escribir? enfoques tras las pr&aacute;cticas    pedag&oacute;gicas, <strong>Viviana Galdames </strong>(Universidad Alberto Hurtado), Lorena    Medina, Ernesto San Mart&iacute;n (Pontificia Universidad Cat&oacute;lica de Chile), Rosa    Gaete (UAH) y Andrea Valdivia (PUC)<br>
      <em>Moderadora:    Carmen Sotomayor</em></li></ol> </td>
    <td width="30%" valign="top"><strong>POL&Iacute;TICA EDUCATIVA 2</strong><br>
            <ol><li> La Econom&iacute;a    pol&iacute;tica del tama&ntilde;o de los&nbsp; colegios:    Evidencia de zonas rurales de Chile, <strong>Francisco Gallego</strong>, Carlos    Rodr&iacute;guez, Enzo Sauma (Pontificia Universidad Cat&oacute;lica de Chile)</li>
            <li> Distance to school and    competition in the Chilean schooling system, R&oacute;mulo Chumacero (Universidad de    Chile), Francisco Meneses (Ministerio de Educaci&oacute;n), Ricardo Paredes    (Pontificia Universidad Cat&oacute;lica de Chile) 
            <strong>Sergio Urz&uacute;a </strong>(Northwestern University, IZA y NBER)</li>
            <li> La decisi&oacute;n de    regresar a la escuela de ni&ntilde;as pertenecientes a familias de bajos recursos.    Estimaci&oacute;n estructural y an&aacute;lisis de pol&iacute;ticas usando la base de datos de    PROGRESA, <strong>Nieves    Vald&eacute;s </strong>(Universidad de Santiago de Chile)</li>
            <li> Gender peer    effects in school: does gender of peers affect student&acute;s achievement?, <strong>Ver&oacute;nica    Cabezas</strong> (Pontificia Universidad Cat&oacute;lica de Chile)<br>
            <em>Moderador:    Juan Eduardo Garc&iacute;a-Huidobro</em> </td>
    <td width="30%" valign="top"><strong>DOCENTES 4</strong><br>
            <ol><li> La formaci&oacute;n    inicial de docentes de educaci&oacute;n general b&aacute;sica en Chile &iquest;qu&eacute; se espera que    aprendan los futuros profesores en el &aacute;rea de lenguaje y comunicaci&oacute;n?, <strong>Carmen    Sotomayor </strong>(Universidad de Chile), Giovanni Parodi (Pontificia Universidad    Cat&oacute;lica de Valpara&iacute;so) Carmen Julia Coloma (Universidad de Chile), Romualdo    Ib&aacute;&ntilde;ez (PUCV), Paula Cavada (Universidad de Chile)</li>
            <li> Mejoramiento de habilidades    cognitivas en universitarios: resultados de una intervenci&oacute;n mediada, <strong>Natalia    Salas, </strong>Alejandra Morales, Rita Ar&eacute;valo, Cecilia Assael (Universidad Diego    Portales)</li>
            <li>Fortalecimiento de la formaci&oacute;n docente inicial:  principales lineamientos y desaf&iacute;os a considerar en un sistema de monitoreo de  los aprendizajes de los estudiantes de pedagog&iacute;a, <strong>Juan    Acevedo </strong>(Pontificia Universidad Cat&oacute;lica de Chile), Tamara Rozas    (CIAE)</li>
            <li>Procesos de reflexi&oacute;n en el    contexto de la formaci&oacute;n inicial docente de profesores de biolog&iacute;a y ciencias    naturales de la Universidad Cat&oacute;lica de la Sant&iacute;sima Concepci&oacute;n, <strong>Alejandra    Nocetti</strong> (Universidad Cat&oacute;lica de la Sant&iacute;sima Concepci&oacute;n)<br>
            <em>Moderador:    Cristi&aacute;n Bellei</em></li></ol></td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="top"><strong>13:30 - 15:00</strong></td>
    <td  nowrap colspan="3"><strong>Almuerzo (libre)</strong></td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="top"><strong>15:00 - 16:30</strong></td>
    <td width="29%" valign="top"><strong>ENSE&Ntilde;ANZA Y APRENDIZAJE 4</strong><br>
            <ol><li> La urdiembre    en la ense&ntilde;anza: expresiones articuladoras del contenido matem&aacute;tico y su    relaci&oacute;n con el desempe&ntilde;o docente, <strong>Himmbler Olivares</strong>, Paloma Opazo,    Tom&aacute;s Arellano, David Silva y Carlos Cornejo (Pontificia Universidad Cat&oacute;lica    de Chile)</li>
            <li> &iquest;Importan las interacciones    entre alumnos para aprender m&aacute;s y&nbsp;    mejor? Revisitando el tema de los efectos pares en Chile y su    relaci&oacute;n&nbsp; con las dimensiones    subjetivas de la vida escolar, <strong>Rodrigo Roco </strong>(IREDU/CNRS Francia)</li>
            <li> Uso del tiempo e interacciones    en la sala de clases, <strong>Sergio Martinic </strong>(Pontificia Universidad Cat&oacute;lica    de Chile), Claudia Vergara (Universidad Cat&oacute;lica Silva Henr&iacute;quez), Marco    Villata (Universidad de Santiago de Chile), David Huepe (Universidad Diego    Portales)</li>
            <li> Estudio sobre los significados    que poseen los estudiantes de 6&ordm; y 7&ordm; b&aacute;sico de tres establecimientos    educacionales de la comuna de Valpara&iacute;so con respecto a la violencia escolar    entre pares. Roc&iacute;o  Calder&oacute;n, Jennifer Mu&ntilde;oz, Yessica Mu&ntilde;oz, <strong>Karen Lobos</strong> (Universidad de Valpara&iacute;so)<br>
            <em>Moderador:    Fidel Ote&iacute;za</em></li></ol></td>
    <td width="30%" valign="top"><strong>GESTI&Oacute;N EDUCATIVA</strong><br>
            <ol><li> &iquest;Logran las    escuelas vulnerables instalar procesos de mejoramiento al instalar asistencia    t&eacute;cnica educativa?, <strong>Ana Rojas</strong></li>
            <li> El entorno organizacional y la    implementaci&oacute;n de pol&iacute;ticas y programas educativos. Un an&aacute;lisis desde las    pr&aacute;cticas de los actores municipales de la Regi&oacute;n del R&iacute;o Maule, <strong>Giselle    Davis </strong>(Universidad de Talca)</li>
            <li> Pr&aacute;cticas de liderazgo    directivo en Chile: evidencia reciente, Jos&eacute; Weinstein, <strong>Gonzalo Mu&ntilde;oz, </strong>Andrea    Horn, Javiera Marf&aacute;n (Fundaci&oacute;n Chile -CEPPE)</li>
            <li> Liderazgo directivo y resultados    de los estudiantes: evidencia a partir de la asignaci&oacute;n de desempe&ntilde;o    colectivo. Un an&aacute;lisis de las cohortes 2005-2008, <strong>M&ordf; Paola Sevilla </strong>(Fundaci&oacute;n Chile)<br>
            <em>Moderador:    Jos&eacute; Weinstein</em></li></ol></td>
    <td width="30%" valign="top"><strong>EDUCACI&Oacute;N SUPERIOR 5</strong><br>
            <ol><li> Brechas en    institucionalidad y financiamiento universitario en Chile, <strong>Carlos C&aacute;ceres </strong>(Universidad    de Chile)</li>
            <li> Ayuda Financiera y Matr&iacute;cula en la Educaci&oacute;n Superior en Chile, Francisco Meneses (Ministerio de Educaci&oacute;n) <strong>Christian    Blanco </strong>(Universidad de Chile)</li>
            <li> Contratos de capital humano en    Chile: ejercicio con datos de Futuro Laboral, <strong>Felipe Lozano </strong>(Lumni    Research)<br>
            <em>Moderador:    Patricio Ort&uacute;zar</em></li></ol> </td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="bottom"><strong>16:30 - 17:00</strong></td>
    <td  nowrap colspan="3"><strong>Caf&eacute;</strong></td>
  </tr>
  <tr>
    <td width="8%" nowrap valign="top"><strong>17:00 - 18:30</strong></td>
    <td  colspan="3" bgcolor="#E9E9E9"><strong>CIIE/CIIES - PANEL EDUCACI&Oacute;N EN EL BICENTENARIO<br>
    Silvina Gvirtz</strong> (Argentina), <strong>Jos&eacute; Joaqu&iacute;n Brunner</strong> (Chile), <strong>Margarita Pe&ntilde;a</strong> (Colombia), <strong>Carlos Ornelas</strong> (M&eacute;xico)</td>
  </tr>
</table>
