Alejandra Mizala, Presidenta del Segundo Congreso Interdisciplinario de Investigaci�n en Educaci�n y co-organizadora del Tercer Congreso de Investigaci�n en Educaci�n Superior, certifica por medio de la presente que <b>{nombre}</b> asisti� como oyente a dicha actividad, realizada los d�as 23 y 24 de agosto de 2012 en Santiago de Chile.

Se extiende el presente certificado para los fines que se estimen convenientes.
