 
<br />
<p>Estimado/a Usuario/a, bienvenido/a al <strong>{email_sitio_nombre}</strong>. </p>

<p>A lo largo de las 5 fases de postulaci&oacute;n al registro ATE, le  pediremos que adjunte una serie de documentos digitalizados en formato PDF. &nbsp;Si no cuenta con el programa Acrobat PDF  instalado en su equipo, puede conseguirlo gratuitamente aqu� <a href="http://www.adobe.com/es/products/reader/"><img src="cea/images/filetypes/pdf.gif" border="0"></a><br />
Si no cuenta con un convertidor de PDF puede bajar un convertidor gratu&iacute;to <a href="http://sourceforge.net/project/downloading.php?groupname=pdfcreator&filename=zPDFCreator-0_9_3-AD_DeploymentPackage-WithoutToolbar.msi&use_mirror=ufpr" target="_blank">aqu&iacute;</a>
</p>
<p>Para evitar perdidas de tiempo y realizar su registro  r&aacute;pidamente y de una sola vez, recomendamos tener a mano la siguiente documentaci&oacute;n:</p>
<br />
<h4> Identificaci&oacute;n Legal: </h4><br />
<table border="1" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td rowspan="2" class="fieldset_title" valign="top" align="center"> Para Acreditar la personalidad    jur&iacute;dica de la     Organizaci&oacute;n / Instituci&oacute;n: </td>
    <td rowspan="2"   width="20%" class="fieldset_title" valign="top"  align="center"> Observaciones </td>
    <td colspan="2"   width="10%"  class="fieldset_title" valign="top" align="center"> Estado</td>
  </tr>
  <tr>
    <td  valign="top" class="fieldset_title"  width="10%" align="center"> P </td>
    <td  valign="top" class="fieldset_title" width="5%" align="center">  C </td>
  </tr>
  <tr>
    <td valign="top"><p>Para    universidades, copia legalizada ante notario de los estatutos de la entidad.    (OK)</p></td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><p>Para las    Universidades privadas, copia legalizada ante notario del Decreto de    reconocimiento, emanado del MINEDUC. (OK)</p></td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" valign="top"  class="fieldset_title" >Para instituciones de educaci&oacute;n    superior:</td>
  </tr>
  <tr>
    <td valign="top"><p>Documento    que certifique acreditaci&oacute;n de la Universidad &nbsp;y su duraci&oacute;n. Adem&aacute;s, se debe incluir una </p></td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><p>Carta de    patrocinio de la     Universidad, certificando que avala la postulaci&oacute;n de la Instituci&oacute;n al    registro de Asistencia T&eacute;cnica Educativa .</p></td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" valign="top" class="fieldset_title" >Para Acreditar vigencia de la    sociedad:</td>
  </tr>
  <tr>
    <td valign="top"><p>Certificado    de vigencia de la     Persona Jur&iacute;dica, emanado de la autoridad competente,    emitido con una fecha que no exceda a los 6 meses (o tiempo a convenir)    anteriores a la presentaci&oacute;n de la solicitud de incorporaci&oacute;n al registro.</p></td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><p>Copia    legalizada ante notario del certificado de socio de la instituci&oacute;n de las    personas que corresponda (al menos, director y equipo permanente)</p></td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" valign="top" class="fieldset_title" >Para Acreditar que dispone de una    oficina administrativa permanente: Criterios a homologar con Chileproveedores</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
</table>

<br /><h4> Identificaci&oacute;n Financiera: </h4><br />

<table border="1" cellspacing="0" width="100%" cellpadding="0">
  
  
  <tr>
    <td rowspan="2" class="fieldset_title" valign="top" align="center"> Criterios Chileproveedores </td>
    <td rowspan="2"   width="20%" class="fieldset_title" valign="top"  align="center"> Observaciones </td>
    <td colspan="2"   width="10%"  class="fieldset_title" valign="top" align="center"> Estado</td>
  </tr>
  <tr>
    <td  valign="top" class="fieldset_title"  width="10%" align="center"> P </td>
    <td  valign="top" class="fieldset_title" width="5%" align="center">  C </td>
  </tr>  
  <tr>
    <td valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
</table>

<br /><h4> Oferta de {nombre_registro} </h4><br />
<table border="1" cellspacing="0" cellpadding="0"> 
  
  <tr>
    <td rowspan="2" class="fieldset_title" valign="top" align="center"> Oferta de {nombre_registro}: </td>
    <td rowspan="2"   width="20%" class="fieldset_title" valign="top"  align="center"> Observaciones </td>
    <td colspan="2"   width="10%"  class="fieldset_title" valign="top" align="center"> Estado</td>
  </tr>
  <tr>
    <td  valign="top" class="fieldset_title"  width="10%" align="center"> P </td>
    <td  valign="top" class="fieldset_title" width="5%" align="center">  C </td>
  </tr>
  <tr>
    <td valign="top"><p>Carta de    referencia </p></td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">Capital Humano Permanente de la Instituci&oacute;n Para    servicios&nbsp; ATE en el Marco de la Ley    SEP:</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><p>Certificado    de t&iacute;tulo de cada uno de los profesionales que integran su oferta ATE.</p></td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
    <td  valign="top">&nbsp;</td>
  </tr>
</table>
<strong>P</strong>: pendiente<br />
<strong>C</strong>: conseguido

<p>Imprima este  check list y revise si ya cuenta con cada un de los de los documentos para  iniciar su registro. <br>
  Le deseamos  el mejor de los &eacute;xitos!!!!<br>
  <strong>&ldquo;Imprimir&rdquo;
